<?php
ini_set('display_errors', 1);
require_once('core/init.php');
require_once('vendor/autoload.php');


$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader);
$db = new MySQLDatbase();

if(!empty($_POST['name']) && !empty($_POST['server'])){ 

/**
 * Wyswietlanie danych gracza
 */
$summoner_name = $_POST['name'];
$server = $_POST['server'];    
$player = new Player($server);
$player_info = $player->get_summoner_info($summoner_name);
$check_if_player_exists = $player->check_user();



if(Curl::$checkUser == FALSE){
        
header('Location: index.php');
}



$player_id = $player_info->id;
$player_stats = $player->get_summoner_stats($player_id);




if($player_stats == NULL){
    $blad = 'Brak danych do wyswietlenia';
}else{
    

    
$player_stats2 = json_decode(json_encode($player_stats), true);
foreach ($player_stats2 as $k => $v) {
    if( $v['id'] == 0) unset($player_stats[$k]);
}
foreach ($player_stats as $player_statistics){
   
    $player_statistics->stats->full_name =  $db->display_champion_info('champion_name',$player_statistics->id );
    
    $player_statistics->stats->full_image =  $db->display_champion_info('champion_image',$player_statistics->id );
    
}
}


$summoner_league = $player->get_summoner_league($player_id);
if($summoner_league == NULL){
    $blad_liga = 'Gracz nie rozegral gier rankingowych';
}else{

foreach($summoner_league as $ranked_stats){
    
    $league_name = $ranked_stats->name;
    $league_tier = $ranked_stats->tier;
    $league_queue = $ranked_stats->queue;
    $league_statistics = $ranked_stats->entries;
    }
}
    
  
$league_icon = $player->get_league_icon($league_tier);    
$player_champions = $player->get_summoner_champions($player_id);


foreach($player_champions as $player_champion){
    
    $player_champion->name = $db->display_champion_info('champion_name',$player_champion->championId );
    
    $player_champion->image = $db->display_champion_info('champion_image',$player_champion->championId );
    
}

$free_champions = $player->get_free_champions();

foreach($free_champions as $free_champion){
    $free_champion->name =   $db->display_champion_info('champion_name',$free_champion->id );
    
    $free_champion->image =  $db->display_champion_info('champion_image',$free_champion->id );
}

$random_runes = $db->display_random_runes(18);



}else{
    
    header('Location: index.php');
}
    
   
    

echo $twig->render('summoner.html.twig',['player' => $player_info, 'server' => $server, 'stats' => $player_stats, 'league_name' => $league_name, 'league_tier' => $league_tier,
        'league_queue' => $league_queue, 'league_statistics' => $league_statistics, 'league_icon' => $league_icon, 'player_champion' => $player_champions, 'free_champions' => $free_champions,
         'random_runes' => $random_runes, 'blad' => $blad, 'blad_liga' => $blad_liga]);

