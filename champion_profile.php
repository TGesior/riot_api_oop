<?php
require_once('core/init.php');
require_once('vendor/autoload.php');



$loader = new Twig_Loader_Filesystem('views/');

$twig = new Twig_Environment($loader);


$db= new MySQLDatbase();

$champion_info = $db->display_champion_profile($_GET['id']);

$random_champions = $db->display_random_champions(4);


echo $twig->render('champions_profile.html.twig',['champion_info' => $champion_info, 'random_champion' => $random_champions]);
