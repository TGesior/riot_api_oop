<?php
require_once('init.php');

class MySQLDatbase
{

private $connection;


function __construct(){ 
   $this->connection = $this->set_connection();
}



private function set_connection(){
    
    $connection =  mysqli_connect(DB_HOST, DB_NAME, DB_PASS, DB_NAME);
    $this->test_connection($connection);
    
    return $connection;
}    
    
private function test_connection($connection){
    if(mysqli_errno($connection) != 0){
        throw new Exception('Blad polaczenia z baza danych');
        
    }
}    
    
private function confirm_query($result){
    if(!$result){
        throw new Exception('Blad zapytania bazy danych');
    }
}    
    
public function query($sql){
    
    $query = mysqli_query($this->connection, $sql);
    $this->confirm_query($query);
    
    return $query;
}    

private function fetch_query($query){
    return mysqli_fetch_object($query);
    
}


public function escape_string($string){
    
    return mysqli_real_escape_string($this->connection,$string);
}


public function display_random_runes($limit)
{
    
    $query = mysqli_query($this->connection, "SELECT * FROM runes ORDER BY runes_tier LIMIT {$limit}");
    $results = array();
    while($result = $this->fetch_query($query)){
        
        $results[] = $result;
        
    }
    
    
    
    return $results;
}

public function display_champion_info($row_name, $value)
{
    
    $query = $this->query("SELECT {$row_name} FROM champions WHERE champion_id = '{$value}' ");
    
    $result = $this->fetch_query($query);
    
    return $result;
    
}

public function display_all_champions(){
    
    $query = "SELECT * FROM champions";
    
    $query = $this->query($query);
    
    $results = array();
    
    while ($result = $this->fetch_query($query)){
        $results[] = $result;
    }
    
    return $results;
}

public function display_champion_profile($champion_id){
    
    $query = $this->query("SELECT * FROM champions WHERE champion_id = {$champion_id}");
    
    $result = $this->fetch_query($query);
    
    return $result;
    
}

public function display_random_champions($limit){
    
    $query = $this->query("SELECT * FROM champions LIMIT {$limit}");
    
    $results = array();
    
    while ($result = $this->fetch_query($query)){
        $results[] = $result;
    }
    
    return $results;
    
}

}

