<?php



 class Player extends RiotApi{
     
/**
 * Definiowanie adresow URL
 */    
const API_URL_1_4     = 'https://{region}.api.pvp.net/api/lol/{region}/v1.4/';
const API_URL_1_2     = 'https://{region}.api.pvp.net/api/lol/{region}/v1.2/';
const API_URL_1_3     = 'https://{region}.api.pvp.net/api/lol/{region}/v1.3/';
const API_URL_2_5     = 'https://{region}.api.pvp.net/api/lol/{region}/v2.5/';
const CHAMPIONMASTERY = 'https://{region}.api.pvp.net/championmastery/location/{region1}/';
/**
 * Zmienna przechowywująca informacje o serwerze gracza
 * @var type $_POST
 */    
protected $server;
protected $player_id;


function __construct($server){
    $this->server = $server;
}



 public static function set_server_name($server_name)
    {
        
        $region = array(
            "NA" => "NA1",
            "EUW" => "EUW1",
            "EUNE" => "EUN1",
            "KR" => "KR",
            "OCE" => "OC1",
            "BR" => "BR1",
            "LAN" => "LA1",
            "LAS" => "LA2",
            "RU" => "RU",
            "TR" => "TR1"
        );
        return $region[$server_name];
    }

public  function check_user()
{
    if(Curl::$requestResponse == 200){
        Curl::$checkUser = TRUE;
    }else{
        Curl::$checkUser = FALSE;
    }
}



public function get_summoner_champions($player_id){
 
    
    $call = 'player/' . $player_id . '/topchampions?count=5&';
    $call = self::CHAMPIONMASTERY . $call . API_KEY;
    
    return $this->json_prepare($call);
    
    
    
}





/**
 * Funckja pobiera informacje dot. gracza // Wartosci: id, name, profileIconId, revisionDate, summonerLevel
 * @param type $name
 * @return type Object JSON
 */
public function get_summoner_info($name)
{
    
    $call = 'summoner/by-name/' . RiotApi::raw_url($name) . '?' . API_KEY;
    $call =self::API_URL_1_4 . $call;
    $name = RiotApi::lower_string($name);
    
    
     return $this->json_prepare($call)->$name;
    
}
/**
 * Funkcja pobiera statystyki gracza weglug postaci
 * @param type $player_id
 * @return type array of Objects
 */
public function get_summoner_stats($player_id)
{
    
    $call = 'stats/by-summoner/' . $player_id . '/ranked?season=SEASON2016&' . API_KEY;
    $call = self::API_URL_1_3 . $call;
    
        
    return $this->json_prepare($call)->champions;
 
}
/**
 * Funkcja zwraca statystyki gier rankingowych gracza
 * @param type $player_id
 * @return type Object
 */
public function get_summoner_league($player_id){
    
    $call = 'league/by-summoner/' . $player_id . '/entry?' . API_KEY;
    $call = self::API_URL_2_5 . $call;    
    
    return $this->json_prepare($call)->$player_id;
}
/**
 * Funkcja zwraca sciezke do pliku z ikona ligi
 * @param type $league
 * @return type string
 */
public function get_league_icon($league_name)
{
   
$league = array(
    
    'BRONZE'     => 'BRONZE.png',
    'SILVER'     => 'SILVER.png',
    'GOLD'       => 'GOLD.png',
    'PLATINUM'   => 'PLATINUM.png',
    'DIAMOND'    => 'DIAMOND.png',
    'MASTER'     => 'MASTER.png',
    'CHALLENGER' => 'CHALLENGER.png'  
);    
    
 array_key_exists($league_name, $league)  ? $league_icon = IMG_PATH .$league[$league_name] : $league_icon = IMG_PATH . 'UNKNOWN.png';
 
 return $league_icon;
    
}



public  function get_free_champions(){
 
    $call = 'champion?freeToPlay=true&';
    $call = self::API_URL_1_2 . $call . API_KEY;
    
    return $this->json_prepare($call)->champions;
    
}




/**
 * Funkcja wstawia server gracza do zapytan url
 * @param type $region array
 * @return type string
 */     
private function set_region($region){
    
    
    if(strpos($region, '{region}') !== FALSE && strpos($region, '{region1}') !== FALSE )
    {     
        
        $values = array('{region}', '{region1}');
        
        $server_name = array(RiotApi::lower_string($this->server), $this->set_server_name($this->server) );
        
        
        return str_replace($values, $server_name, $region); 
        
 
    } else if(strpos($region, '{region}') !== FALSE) 
    {        

        return str_replace('{region}', RiotApi::lower_string($this->server), $region);   
        
        
    } else if(strpos($region, '{region1}') !== FALSE)
    {
        
        return str_replace('{region1}', $this->set_server_name($this->server), $region);    
        
        
    }
    
    return $region;
    
    
}

/**
 * Zamiana kodu JSON na obiekty
 * @param type $url
 * @return type Object
 */
private function json_prepare($url)
{
    
    $url = $this->set_region($url);
    
    $file = self::get_contents($url);
    
    return json_decode($file);
}    
     

public function test($arr){
    echo '<pre>';
    var_dump($arr);
    echo '</pre>';
}


 }
