<?php

require_once('init.php');

class Curl{

/**
 * Zmienna przechowywujaca bledy polaczenia z RIOT API
 * @var type array
 */

public static $requestResponse;

public static $checkUser = '';

    
private static $errorCodes = array(
    
    0 => 'BRAK_ODPOWIEDZI',
    400 => 'NIEPRAWIDLOWE_ZAPYTANIE',
    401 => 'BRAK_AUTORYZACJI',
    403 => 'ODMOWA_DOSTEPU',
    404 => 'NIE_ZNALEZIONO_PLIKU',
    429 => 'PRZEKROCZONO_LIMIT_TRANFERU',
    500 => 'BLAD_SERVERA',
    503 => 'NIEDOSTEPNE');

/**
 * Pobieranie kodu JSON z serwera
 * @param type $url
 * @return type JSON
 */
public static function curl_get_content($url)
{
    
    $start = curl_init();
    curl_setopt($start, CURLOPT_URL, $url);
    curl_setopt($start,CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($start);
    
    self::$requestResponse = curl_getinfo($start, CURLINFO_HTTP_CODE);
    curl_close($start);

    if(self::$requestResponse != 200 && self::$checkUser == TRUE){
        
        
        //throw new Exception('Blad polaczenia :'. self::$errorCodes[self::$requestResponse] );
        return NULL;
           
    }
    
    return $result;
}    


    
    
    
    
}