<?php

class RiotApi
{
    


/**
 * Zmienna tablicowa z bledami servera
 * @var type array
 */
private static $errorCodes = array(
    
        400 => 'ZLY_ADRES',
        401 => 'BRAK_AUTORYZACJI',
        403 => 'ODMOWIONO_DOSTEPU',
        404 => 'NIE_ZNALEZIONO_ADRESU',
        429 => 'PRZEKROCZONO_LIMIT_TRANSFERU',
        500 => 'BLAD_SERVERA',
        503 => 'NIEDOSTEPNE'

    );

/**
 * Funcja pobiera kod JSON
 * @param type $url
 * @return type JSON
 */
public static function get_contents($url)
{
    
return Curl::curl_get_content($url);
    
}    



/**
 * Zamiana stringow na duze litery
 * @param type $string
 * @return type string
 */
public static function upper_string($string)
{
    
    return strtoupper($string);
}    

/**
 * Zamienia string na male litery
 * @param type $string
 * @return type string
 */
public static function lower_string($string)
{
    
    return strtolower($string);
    
}        

/**
 * Przygotwanie strigu do umieszczenia w adresie url
 * @param type $url
 * @return type sring
 */
public static function raw_url($url)
{
 
    return rawurlencode($url);
    
}


    
}

